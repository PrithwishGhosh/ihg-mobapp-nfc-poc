package com.ihg.app.android;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.ihg.app.R;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setDisplayShowCustomEnabled(true);
            getSupportActionBar().setCustomView(R.layout.header_nav_drawer);

            View view = getSupportActionBar().getCustomView();
            view.findViewById(R.id.icon_drawer).setVisibility(View.GONE);
            view.findViewById(R.id.text_sign_in_name).setOnClickListener(this);
            view.findViewById(R.id.icon_profile_notification).setOnClickListener(this);
        }
    }

    public void onClickFindHotel(View view) {
        Toast.makeText(getApplicationContext(), "find hotels clicked", Toast.LENGTH_SHORT).show();
    }


    public void login(View view) {
        Toast.makeText(getApplicationContext(), "login clicked", Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    public void showNotifications(View view) {
        Toast.makeText(getApplicationContext(), "notify clicked", Toast.LENGTH_SHORT).show();
    }

    public void activateT20Service(View view) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.t20_popup_dialog);
        dialog.show();
        Button button = dialog.findViewById(R.id.button_ok);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view != null) {
            if (view.getId() == R.id.text_sign_in_name) {
                login(view);
            } else if (view.getId() == R.id.icon_profile_notification) {
                showNotifications(view);
            }
        }
    }
}
