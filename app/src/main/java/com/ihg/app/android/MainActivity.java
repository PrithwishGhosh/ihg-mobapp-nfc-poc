package com.ihg.app.android;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ihg.app.R;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    private String dummyName = "Christopher";
    private String dummyPoints = "224712571";
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.mDrawerLayout = findViewById(R.id.drawer_layout);

        // Setup Toolbar and Navigation Menu
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        // We will shwo custom toolbar here so hide the default one
        if (getSupportActionBar() != null)
            getSupportActionBar().hide();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.nfc);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(MainActivity.this);
                dialog.setContentView(R.layout.t20_popup_dialog);
                dialog.show();
                Button button = dialog.findViewById(R.id.button_ok);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
            }
        });


        showUserInfoOnNavMenu(navigationView, dummyName, dummyPoints);
        showUserInfoOnToolbar(dummyName);

    }

    /**
     * Display user details in toolbar and add events.
     * @param name
     */
    private void showUserInfoOnToolbar(final String name) {
        findViewById(R.id.icon_drawer).setOnClickListener(this);
        findViewById(R.id.icon_profile_notification).setOnClickListener(this);
        final TextView tvSignInName = findViewById(R.id.text_sign_in_name);
        tvSignInName.setOnClickListener(this);
        tvSignInName.setText(name);
    }

    /**
     * Populate Navigation Menu info
     * @param navView
     */
    private void showUserInfoOnNavMenu (final NavigationView navView, String name, String point) {
        final View headerView = navView.getHeaderView(0);
        final TextView tvUserName = headerView.findViewById(R.id.drawer_text_name);
        final TextView tvUserClubPoint = headerView.findViewById(R.id.drawer_text_club_point);
        tvUserName.setText(name);
        tvUserClubPoint.setText(point);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onClick(View view) {
        final int viewId = view.getId();
        switch (viewId) {
            case R.id.icon_profile_notification:
                Toast.makeText(getApplicationContext(), "Profile Clicked", Toast.LENGTH_SHORT).show();
                break;
            case R.id.icon_drawer:
                mDrawerLayout.openDrawer(Gravity.START);
                break;
        }
    }

    public void onClickFindHotel(View view) {
        Toast.makeText(getApplicationContext(), "Find Hotel Clicked", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        if (item != null) {
            switch (item.getItemId()) {
                case R.id.nav_find_hotel:
                    Toast.makeText(this, "Find Hotels", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.nav_stays:
                    Toast.makeText(this, "Stays", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.nav_offers:
                    Toast.makeText(this, "My Offers", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.nav_experience:
                    Toast.makeText(this, "Experience", Toast.LENGTH_SHORT).show();
                    break;
                case R.id.nav_home:
                    Toast.makeText(this, "Home", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(this, LoginActivity.class);
                    startActivity(intent);
                    break;
            }

            mDrawerLayout.closeDrawer(GravityCompat.START);
            return true;
        }
        return false;
    }
}
